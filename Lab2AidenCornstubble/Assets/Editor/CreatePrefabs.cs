﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

public class CreatePrefabs : MonoBehaviour {

	[MenuItem("Project Tools/Create Prefab")]
  public static void CreatePrefab()
  {
    GameObject[] selectedObjects = Selection.gameObjects;

    foreach(GameObject go in selectedObjects)
    {
      string name = go.name;
      //string assetPath = path + "/" + name + ".prefab";
      string assetPath = EditorUtility.SaveFilePanelInProject("Save prefabs to folder",  go.name + ".prefab", "prefab", "");

      if(AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)))
      {
        if(EditorUtility.DisplayDialog("Caution", "Prefab " + name + " already exists.  Do you want to overwrite", "Yes", "No"))
        {
          Debug.Log("overwritting!");
          CreateNew(go, assetPath);
        }
      }
      else
      {
        CreateNew(go, assetPath);
      }

      //Debug.Log("Name: " + go.name + " Path: " + assetPath);
    }
  }

  public static void CreateNew(GameObject obj, string location)
  {
    Object prefab = PrefabUtility.CreateEmptyPrefab(location);
    PrefabUtility.ReplacePrefab(obj, prefab);
    AssetDatabase.Refresh ();

    DestroyImmediate (obj);
    GameObject clone = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
  }
}
